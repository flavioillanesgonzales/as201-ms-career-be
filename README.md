# **Técnicas y Buenas Prácticas de Clean Code en los Microservicios de PRS**

## **1) Variables**

- Uso de nombres de variables significativos y pronunciables al momento de crear los modelos en el Back-end de Spring Boot y el Front-End de Angular.

![](images/1.png)
![](images/2.png)

## **2) Favorecer la programación funcional sobre la programación imperativa**

- Con la implementación de la librería Lombok, nos permite tener getters, setters, constructores, etc. Lombok se encarga de todo esto tras inyectar su código sobre el nuestro, además es totalmente instantáneo para el desarrollador.

![](images/3.png)
![](images/4.png)

## **3) Preferir la composición a la herencia**

- Usamos la composición de herencia de crud reactiva en la interfaz de repository.

![](images/5.png)

## **4) Los nombres de las métodos deben decir lo que hacen**

- No hace falta colocar un comentario para explicar que hace el método, el nombre del método debe hablar por sí solo.

![](images/6.png)

## **5) No comentar el código**

- No dejar código comentado diario, ni conservar métodos antiguos, cuando se ejecuta el proyecto las líneas de código comentadas son ignoradas, es igual a que si estuvieran borradas.

![](images/7.png)

## **6) Evitar los marcadores de posición**

- Estos marcadores son usados comunmente con los comentarios, como por ejemplo:

![](images/8.png)

## **7) Eliminar código duplicado**

- Por ejemplo en los métodos de actualizar y el borrado lógico.

![](images/9.png)
![](images/10.png)

## **8) Evitar las condicionales**

- Evitar usar las condicionales en los métodos, ya sea switch o if, en este caso if.

![](images/11.png)

## **9) Evitar la verificación de datos**

- Evitar la verificación de datos, por ejemplo con las condicionales, ya sea con “y” (&&) u “o” (||).

![](images/12.png)

## **10) Los métodos deben hacer una cosa**

- Los métodos solo deben de hacer una cosa, por ejemplo en este caso se incumple y realiza varias cosas.png

![](images/13.png)

# **Técnicas y Buenas Prácticas en los Test con Clean Code en los Microservicios de PRS**

## **1) Aplicar bien las  anotaciones y las inyecciones  de dependencias**

## **2) Saber instanciar bien las clases, llamándolo con valores entendibles**

![](images/14.png)

## **3) Llamar bien los métodos que tenemos  en nuestra Entidad Modelo**

![](images/15.png)
![](images/16.png)

## **Bibliografía**

- __[GitHub - Clean Code](https://github.com/ryanmcdermott/clean-code-javascript#clean-code-javascript)__
