-- CREAR DATABASE as201-ms-career
CREATE DATABASE "as201-ms-career"
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Spanish_Peru.1252'
    LC_CTYPE = 'Spanish_Peru.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;


-- COMENTAR DATABASE
COMMENT ON DATABASE "as201-ms-career"
    IS 'Career Microservice Database';


-- CREAR TABLA career
CREATE TABLE career (
	id SERIAL PRIMARY KEY NOT NULL,
	name VARCHAR(50) NOT NULL,
	boss VARCHAR(100) NOT NULL,
	area VARCHAR(100) NOT NULL,
	institute VARCHAR(100) NOT NULL,
	pension DECIMAL(5,2) NOT NULL,
	course INT NOT NULL,
	semester INT NOT NULL,
	status CHAR(1) NOT NULL
);


-- INSERTAR REGISTROS DE UBIGEO
INSERT INTO career
(name,boss,area,institute,pension,course,semester,status)
VALUES
('Análisis de Sistemas','Luis Aquilino Manzo Candela','Carrera Profesional de Anális de Sistemas','I.E.S.T.P Valle Grande','350.00','9','6','A'),
('Producción Agraria','Luis Ángel Ricardo Campos','Carrera Profesional de Producción Agraria','I.E.S.T.P Valle Grande','350.00','9','6','A');


SELECT * FROM career;


/*
SERVIDOR = pgsqlserver123
USUARIO = postgressql
CONTRASEÑA = Java2020@
*/
