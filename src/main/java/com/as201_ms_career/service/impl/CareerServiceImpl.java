package com.as201_ms_career.service.impl;

import com.as201_ms_career.service.CareerService;
import com.as201_ms_career.model.Career;
import com.as201_ms_career.repository.CareerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class CareerServiceImpl implements CareerService {

    @Autowired
    private CareerRepository careerRepository;

    public Flux<Career> findCareer() {
        log.info("Mostrando todas las carreras");
        return careerRepository.findAll();
    }

    public Mono<Career> findCareerById(Long id) {
        log.info("Carrera encontrada con el ID = " + id);
        return careerRepository.findById(id);
    }

    public Flux<Career> findCareerByStatus(String status) {
        return careerRepository.findByStatus(status);
    }

    public Mono<Career> createCareer(Career career) {
        log.info("Carrera creada = " + career.toString());
        career.setStatus("A");
        return careerRepository.save(career);
    }

    public Mono<Career> updateCareer(Career career) {
        log.info("Carrera actualizada = " + career.toString());
        return careerRepository.findById(career.getId()).map((car) -> {
            car.setId(career.getId());
            car.setName(career.getName());
            car.setBoss(career.getBoss());
            car.setArea(career.getArea());
            car.setInstitute(career.getInstitute());
            car.setPension(career.getPension());
            car.setCourse(career.getCourse());
            car.setSemester(career.getSemester());
            car.setStatus(career.getStatus());
            return car;
        }).flatMap(car -> careerRepository.save(car));
    }

    public Mono<Career> deleteCareer(Long id) {
        log.info("Carrera eliminada = " + id);
        Career data = new Career();
        data.setId(id);
        data.setStatus("I");
        return dataCareer(data, id);
    }

    public Mono<Career> restoreCareer(Long id) {
        log.info("Carrera restaurada = " + id);
        Career data = new Career();
        data.setStatus("A");
        return dataCareer(data, id);
    }

    public Mono<Career> dataCareer(Career data, Long id) {
        return careerRepository.findById(id).map((career) -> {
            data.setId(career.getId());
            data.setName(career.getName());
            data.setBoss(career.getBoss());
            data.setArea(career.getArea());
            data.setInstitute(career.getInstitute());
            data.setPension(career.getPension());
            data.setCourse(career.getCourse());
            data.setSemester(career.getSemester());
            data.getStatus();
            return data;
        }).flatMap(career -> careerRepository.save(data));
    }
}
