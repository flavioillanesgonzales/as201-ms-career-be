package com.as201_ms_career.controller;

import com.as201_ms_career.model.Career;
import com.as201_ms_career.service.CareerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@RestController
@CrossOrigin(origins="http://localhost:4200")
@RequestMapping("/v1/career")
public class CareerController {

	@Autowired
	private CareerService careerService;

	@GetMapping
	public Flux<Career> findCareer() {
		return careerService.findCareer();
	}

	@GetMapping("/id/{id}")
	public Mono<Career> findCareerById(@PathVariable Long id) {
		return careerService.findCareerById(id);
	}
	
	@GetMapping("/status/{status}")
	public Flux<Career> findCareerByStatus(@PathVariable String status) { return careerService.findCareerByStatus(status); }

	@PostMapping
	public Mono<Career> createCareer(@RequestBody Career career) {
		return careerService.createCareer(career);
	}

	@PutMapping
	public Mono<Career> updateCareer(@RequestBody Career career) { return careerService.updateCareer(career); }

	@GetMapping("/delete/{id}")
	public Mono<Career> deleteCareer(@PathVariable Long id) { return careerService.deleteCareer(id); }

	@GetMapping("/restore/{id}")
	public Mono<Career> restoreCareer(@PathVariable Long id) {
		return careerService.restoreCareer(id);
	}
}
