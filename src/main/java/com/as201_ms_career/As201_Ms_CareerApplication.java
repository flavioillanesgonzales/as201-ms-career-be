package com.as201_ms_career;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class As201_Ms_CareerApplication {

    public static void main(String[] args) {
        SpringApplication.run(As201_Ms_CareerApplication.class, args);
    }

}
